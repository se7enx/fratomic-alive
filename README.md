# DEPRECATED, use https://gitlab.com/contextualcode/fratomic-alivue instead, as it provides complete Vue3 solution.

# Fratomic Alive

Use fratomic tool to build live single page website that uses Vue and API data.

Optionally, can be deployed with Server-Side Rendering.

# Installation

1. Make sure you have [Vue CLI installed](https://cli.vuejs.org/guide/installation.html).

2. In your fratomic project root directory:
```shell script
npm install --save git+ssh://git@gitlab.com:contextualcode/fratomic-alive#v1.2.1
```

3. In your `fractal.config.js`:
```js
/*
 * Register Vue templates adapter and CLI commands
 */
const { FractalVueAdapter, AliveCli } = require('@contextualcode/fratomic-alive');
fractal.components.set('engine', FractalVueAdapter);
fractal.components.set('ext', '.vue');
new AliveCli(fractal);

// for theme definition, adds "Live preview" button
webUITheme.addLoadPath(uiLibRoot + 'node_modules/@contextualcode/fratomic-alive/webui-mono');
```

# Usage

- Update fratomic preview template.
It should be similar to what you have in live vue project in `src/App.vue`.
You can find base example in [examples/_preview.vue](examples/_preview.vue).

- Update fratomic pagelayout template.
It should be similar to what you have in live vue project in `src/assets/index.server.html`.
You can find base example in [examples/_pagelayout.html](examples/_pagelayout.html).
> Please note only `{{ title }}` variable available in pagelayout.

Make sure you have vue included in your `_pagelayout.html` file. Otherwise, "Live Preview" will not work.
```html
<!-- development version, includes helpful console warnings, required for "live preview" -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
```

- You can use full vue file definition, as in ["examples/address.vue"](examples/components/01-atoms/02-address/address.vue).
Or, you can keep it short with template only, as in ["examples/button.vue"](examples/components/01-atoms/01-buttons/button.vue).
For shortened version, `fratomic-alive` will generate script tag automatically,
based on ["*.config.json"](examples/components/01-atoms/01-buttons/button.config.json) context.

- Update your fratomic components configs to contain `alive` configs where needed.
```json
{
    "alive": {
        "pageIdentifier": "frontpage",
        "pageIdentifier": ["frontpage", "folder"],
        "lazy": false,
        "chunkName": "search",
        "exclude": true,
        "routing": {
            "route": "/searchRequired",
            "name": "optionalName",
            "metaTitle": "(optional) Search page - Project Name",
            "metaKeywords": "(optional) find, search, look for result",
            "metaDescription": "(optional) This page allows search by keyword"
        }
    }
}
```
> `pageIdentifier` can be array or string.
> Defines which page component use for an API response.

> `routing` needed only if you want to display it without API URL.

> `lazy` (default: `true`). Applicable only for pages and routes.
> `true` - loaded on demand. `false` - bundled with main app script.

> `exclude` (default: `false`). If `true` - component excluded from build to live project.

> `chunkName` (default: `null`). Applicable only for lazy pages and routes.
> `null` - unique chunk name generated, so bundled separately from others. 
> `"some-string"` - bundled into named chunk, useful when components placed on the same page.

> All parameters optional, and you can skip `"alive"` too.

- Generate vue project.
```shell script
fratomic boilerplate -o ../relative/path/to/live-vue-project-name
```
> This should be one-time action, feel free to edit generated files.
> In most cases, you'll need update App.vue component to add header, footer, etc.

- Build components from fratomic
```shell script
fratomic buildalive -o ../relative/path/to/live-vue-project-name
```

- Update configuration in `path/to/live-vue-project-name/.env`.
> Tip: You can use `.env.local` for local override.

- Configure links to ignore in `src/handler/PageHandler.js` if needed.
> We ignore links to pdf and image files by default.

- By default, page data loaded from `/api/${window.location.href}` and expects structure like this:
```json
{
    "type": "page type defined in `pageIdentifier`",
    "data": {
        "attribute name used as prop for the Component": "value",
        "metaTitle": "",
        "metaKeywords": "",
        "metaDescription": ""
    }
}
```
> Fields `metaTitle`, `metaKeywords`, `metaDescription` used to build page head tags,
> and removed from props by default.

> If you prefer other API structure for component props or meta, you can customize it in `src/handler/PageHandler.js`.

- Build and start project
```shell script
cd /path/to/live-vue-project-name
npm install
npm run build
npm run start
```

# Styles

- To include styles in fratomic preview, you can add scss in your [_preview.vue](examples/_preview.vue).
```vue
<style lang="scss">
    @import "../src/scss/global.scss";
</style>
```

- To include global styles in vue app, you can use scss in your [src/App.vue](boilerplate/src/App.vue).
```vue
<style lang="scss">
    @import "./fratomic/scss/global.scss";
</style>
```
> Note: You should place all your styles files in `fratomic_folder/src/*`, and use relative path for
> `@import` in fratomic components.

> Build will copy all `fratomic/src/*` files to `live-vue-project/src/fratomic/*`
> (and update pathes in fratomic components).

> If you have assets that should be publicly available, use `postbuild` to copy them, for example:
```json
{
    "scripts": {
        "postbuild": "cp -R src/fratomic/img public/img"
    }
}
```

# Docker usage

- Update `image: registry.gitlab.com/contextualcode/project/express` in `docker/docker-compose.yml`

- Use `express` for SSR version (recommended), or `nginx` to serve without SSR
```shell script
cd /path/to/live-vue-project-name/docker
docker-compose build express
docker push registry.gitlab.com/contextualcode/project/express

docker-compose up express
# or:
docker run -d -p 80:3000 registry.gitlab.com/contextualcode/project/express
```
