import Vue from 'vue'
import VueRouter from 'vue-router'
import FratomicRoutes from "../fratomic/routes"

let routes = FratomicRoutes;

// add custom routes
// routes.push(...[
// 	{
// 		component: About,
// 		name: 'about',
// 		path: '/about'
// 	}
// ])

Vue.use(VueRouter);

export function createRouter() {
	return new VueRouter({
		mode: 'history',
		base: process.env.BASE_URL,
		routes
	})
}
