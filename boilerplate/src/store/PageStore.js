import RequestHandler from '../handler/RequestHandler';
import PageHandler from '../handler/PageHandler';
import pageComponents from "../fratomic/dynamicPageComponents";

export const createPageStore = () => {
	return {
		state: {
			pageProps: {},
			pageComponent: null,
		},
		actions: {
			resetPage(context) {
				return context.commit('resetPage');
			},
			fetchPage(context, request) {
				return RequestHandler.loadPage(request.url)
					.then(response => {
						context.commit('setPage', response);

						return response;
					})
					.catch(error => {
						if (request.redirectOnFail && process.env.VUE_ENV !== 'server') {
							window.location.replace(request.url);
						}

						return error;
					})
			},
		},
		mutations: {
			setPage(state, response) {
				const {pageIdentifier, pageProps} = PageHandler.parsePageResponse(response);
				state.pageComponent = null;
				pageComponents.forEach((component) => {
					if (component.pageIdentifier.includes(pageIdentifier)) {
						state.pageComponent = component.componentName;

						if (component.props && component.props.length) {
							for (let prop in pageProps) {
								if (!component.props.includes(prop)) {
									delete pageProps[prop];
								}
							}
						}
					}
				});
				state.pageProps = pageProps;
			},
			resetPage(state) {
				state.pageProps = {};
				state.pageComponent = null;
			}
		},
		getters: {
			currentPageProps: state => {
				return state.pageProps;
			},
			currentPageComponent: state => {
				return state.pageComponent;
			},
		},
	};
};
