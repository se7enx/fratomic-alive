import Vue from 'vue';
import Vuex from 'vuex';
import { createPageStore } from './PageStore';

Vue.use(Vuex);

export const createStore = () => {
	const PageStore = createPageStore();

	return new Vuex.Store({
		state: {},
		getters: {},
		mutations: {},
		actions: {},
		modules: {PageStore},
	});
};
