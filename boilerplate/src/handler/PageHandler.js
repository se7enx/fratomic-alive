export default new class PageHandler {

	constructor() {
		// ignore links to pdf and image files
		// custom - ignore links to file download URL
		this.ignoreLinks = /(^mailto:)|(^fax:)|(^callto:)|(^tel:)|(\.pdf$)|(\.jpe?g$)|(\.png$)|(\.html$)|(^\/content\/download\/)/i;
	}

	parsePageResponse(response) {
		const pageIdentifier = response.type;
		let pageProps = Object.assign({}, response.data || {});
		delete pageProps.metaTitle;
		delete pageProps.metaKeywords;
		delete pageProps.metaDescription;

		return {pageIdentifier, pageProps};
	}

	updatePageHeadByRoute(route, appComponent) {
		const nearestWithMeta = route.matched.slice().reverse().find(r => r.meta);

		const data = nearestWithMeta ? {
			metaTitle: nearestWithMeta.meta.title || null,
			metaKeywords: nearestWithMeta.meta.keywords || null,
			metaDescription: nearestWithMeta.meta.description || null,
		} : {};
		this.updatePageHead({data: data}, appComponent);
	}

	updatePageHead(apiResponse, appComponent) {
		let title = process.env.VUE_APP_DEFAULT_TITLE;
		if (apiResponse && apiResponse.data) {
			title = apiResponse.data.metaTitle || apiResponse.data.title || apiResponse.data.name || title;
		}
		if (title !== process.env.VUE_APP_DEFAULT_TITLE) {
			if (process.env.VUE_APP_TITLE_PREFIX) {
				title = process.env.VUE_APP_TITLE_PREFIX + title;
			}
			if (process.env.VUE_APP_TITLE_POSTFIX) {
				title += process.env.VUE_APP_TITLE_POSTFIX;
			}
		}

		if (process.env.VUE_ENV === 'server') {
			// we don't need meta tags for users, only robots, feel free to add more SEO info here
			let keywords = process.env.VUE_APP_DEFAULT_KEYWORDS;
			let description = process.env.VUE_APP_DEFAULT_DESCRIPTION;
			if (apiResponse && apiResponse.data) {
				keywords = apiResponse.data.metaKeywords || keywords;
				description = apiResponse.data.metaDescription || description;
			}

			appComponent.$ssrContext.title = title;
			appComponent.$ssrContext.keywords = keywords || '';
			appComponent.$ssrContext.description = description || '';
		} else {
			document.title = title;
		}

		if (typeof window !== 'undefined') {
			window.scrollTo(0, 0);
		}
	}

	catchLinkToRoute(e, appComponent) {
		const linkRoute = this.findLinkRoute(e.target);
		if (linkRoute) {
			e.preventDefault();
			appComponent.navigatedFromLink = true;
			appComponent.$router.push(linkRoute);
		}
	}

	findLinkRoute(node) {
		let maxDepth = 10;
		while (node !== null && maxDepth-- > 0) {
			if (node.tagName && node.tagName.toLowerCase() === 'a') {
				if (node.getAttribute('target') && node.getAttribute('target') !== '_self') {
					return null;
				}
				const href = node.getAttribute('href');
				if (!href || href[0] === '#') {
					return null;
				}
				const matchAbsoluteLink = href.match(new RegExp('^(?:[a-z]+:)?//([A-z0-9:.]*?)', 'i'));
				if (matchAbsoluteLink) {
					if (matchAbsoluteLink[1].toLowerCase() === window.location.host) {
						return href.substring(href.indexOf(matchAbsoluteLink[1]) + matchAbsoluteLink[1].length) || null;
					}
					return null;
				}
				if (this.ignoreLinks && this.ignoreLinks.test(href)) {
					return null;
				}

				return href;
			}
			node = node.parentNode;
		}

		return null;
	}
}
