'use strict';

module.exports = {
	FractalVueAdapter: require('./src/fractal-adapter'),
	AliveCli: require('./src/alive-cli'),
};
