'use strict';

const path = require('path');
const Boilerplate = require('./service/Boilerplate');
const BuildFratomic = require('./service/BuildFratomic');

class AliveCli {
	constructor(fractal) {
		this._fractal = fractal;
		this._boilerplate = new Boilerplate();
		this._buildFratomic = new BuildFratomic();
		this.registerCommands(fractal.cli);
	}

	build(args) {
		const outputRootPath = path.join('./', args.options.output || '');
		this._buildFratomic.buildComponents(outputRootPath, this._fractal.components);
	}

	boilerplate(args) {
		const outputRootPath = path.join('./', args.options.output || '');
		this._boilerplate.boilerplate(outputRootPath);
	}

	registerCommands(cli) {
		cli.command('buildalive', this.build.bind(this), {
			description: 'Build components to project directory',
			options: [
				['-o, --output <output-dir>', 'The directory to export into, relative to the CWD.'],
			]
		});

		cli.command('boilerplate', this.boilerplate.bind(this), {
			description: 'Generates base Vue project files',
			options: [
				['-o, --output <output-dir>', 'The directory to export into, relative to the CWD.'],
			]
		});
	}
}

module.exports = AliveCli;
