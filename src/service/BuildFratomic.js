'use strict';

const path = require('path');
const fs = require('fs');
const FilesystemHelper = require('../helper/FilesystemHelper');
const VueFilesHelper = require('../helper/VueFilesHelper');

const FRATOMIC_SRC_PATH = 'src/fratomic';
const COMPONENTS_PATH = FRATOMIC_SRC_PATH + '/components';

class BuildFratomic {
	buildComponents(outputRootPath, components) {
		this.clearComponentFiles(outputRootPath);
		const componentRootPath = path.join(outputRootPath, COMPONENTS_PATH);

		const items = components.flattenDeep().toArray();
		const routes = [];
		const pageComponents = [];

		console.log(`Copy "src/*" files`);
		if (fs.existsSync(path.join('./', 'src'))) {
			FilesystemHelper.copyFolderRecursiveSync(
				path.join('./', 'src'),
				path.join(outputRootPath, FRATOMIC_SRC_PATH),
			);
		}

		// export vue components and collect routes
		for (const item of items) {
			const configData = item.component().configData;
			const aliveConfig = configData && configData.alive || null;
			if (aliveConfig && aliveConfig.exclude) {
				continue;
			}

			const exportedFileName = VueFilesHelper.uniqueComponentFileName(
				componentRootPath,
				item.alias || item.handle
			);

			// build fratomic component to vue
			let componentContent = VueFilesHelper.fixImport(item.getContentSync());
			componentContent = VueFilesHelper.fixStyleInclude(componentContent);
			componentContent = VueFilesHelper.generateScriptIfNeeded(componentContent, configData, exportedFileName);
			fs.writeFileSync(path.join(componentRootPath, `${exportedFileName}.vue`), componentContent);

			// generate page route or component
			if (aliveConfig) {
				// common config
				const commonConfig = {
					lazy: aliveConfig.lazy !== false, // true by default
					chunkName: aliveConfig.chunkName || null,
				}

				// add route
				if (aliveConfig.routing) {
					routes.push(Object.assign({
						config: aliveConfig.routing,
						fileName: exportedFileName
					}, commonConfig));
				}

				// add dynamic page (CMS route)
				if (aliveConfig.pageIdentifier) {
					pageComponents.push(Object.assign({
						fileName: exportedFileName,
						pageIdentifier: Array.isArray(aliveConfig.pageIdentifier)
							? aliveConfig.pageIdentifier : [aliveConfig.pageIdentifier],
					}, commonConfig));
				}
			}
		}
		console.log(`Register ${items.length} components`);

		// we need empty component for vue routing when CMS routing rendered
		fs.writeFileSync(
			path.join(outputRootPath, COMPONENTS_PATH, `empty.vue`),
			VueFilesHelper.emptyComponent()
		);

		console.log(`Generate ${routes.length} route` + (routes.length !== 1 ? 's' : ''));
		fs.writeFileSync(
			path.join(outputRootPath, FRATOMIC_SRC_PATH, 'routes.js'),
			VueFilesHelper.generateRoutesDefinitionFile(routes)
		);

		console.log(`Generate ${pageComponents.length} page` + (pageComponents.length !== 1 ? 's' : ''));
		fs.writeFileSync(
			path.join(outputRootPath, FRATOMIC_SRC_PATH, 'dynamicPageComponents.js'),
			VueFilesHelper.generateDynamicPageComponentsFile(pageComponents, componentRootPath)
		);
	}

	clearComponentFiles(outputPath) {
		const componentsPath = path.join(outputPath, COMPONENTS_PATH);

		FilesystemHelper.createDirectory(path.join(outputPath, FRATOMIC_SRC_PATH));
		FilesystemHelper.createDirectory(componentsPath);
		// clear previously built components
		FilesystemHelper.deleteFolderRecursive(componentsPath, false);
	}

}

module.exports = BuildFratomic;
