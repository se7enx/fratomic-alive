const path = require('path');
const fs = require('fs');
const sass = require('node-sass');
const VueTemplateCompiler = require('vue-template-compiler');
const Babel = require('@babel/core');
const babelPreset = require('babel-preset-env');

class VueFilesHelper {
	emptyComponent() {
		return '<template><div style="display: none"></div></template>';
	}

	fixImport(content) {
		return content.replace(/import ([A-z0-9_]+) from\ ["']([A-z0-9\/\-\._]+?)["']/g, function (match, className, importFilePath) {
			const fileName = importFilePath.substring(importFilePath.lastIndexOf('/') + 1);
			if (importFilePath === fileName) {
				// don't touch module import, only relative components
				return `import ${className} from "${importFilePath}"`;
			}

			return `import ${className} from "./${fileName}"`;
		});
	}

	fixStyleInclude(content) {
		if (content.indexOf('<style ') === -1) {
			return content;
		}

		// <style src="../../../src/scss/file.scss"> => <style src="../scss/file.scss">
		content = content.replace(/<style (.*?)src=["|'](.*?)["|']/g, function (match, attributes, filePath) {
			filePath = filePath.substring(filePath.indexOf('../src/') + 7);

			return `<style ${attributes}src="../${filePath}"`;
		});

		// @import "../../../src/scss/file.scss" => @import "../scss/file.scss"
		content = content.replace(/@import\s+?["|'](.*?)["|']/g, function (match, filePath) {
			filePath = filePath.substring(filePath.indexOf('../src/') + 7);

			return `@import "../${filePath}"`;
		});

		return content;
	}

	generateComponentCode(componentObject) {
		let properties = [];
		const isArray = Array.isArray(componentObject);

		for (const prop in componentObject) {
			if (prop === '_Ctor') {
				continue; // skip vue cache
			}
			if (!isArray && !componentObject.hasOwnProperty(prop)) {
				continue;
			}

			let varType = typeof componentObject[prop];
			let val;
			if (varType === 'function') {
				val = componentObject[prop].toString();
				if (val.includes('{ [native code] }')) {
					val = componentObject[prop].name; // native code functions, like Boolean, String, etc.
				}
			} else if (varType === 'number' || varType === 'bigint') {
				val = componentObject[prop];
			}else if (varType === 'undefined') {
				val = 'undefined';
			} else if (varType === 'string') {
				val = JSON.stringify(componentObject[prop]);
			} else if (componentObject[prop] === null) {
				val = 'null';
			} else if (varType === 'object') {
				val = this.generateComponentCode(componentObject[prop]);
			} else if (varType === 'boolean') {
				val = componentObject[prop] ? 'true' : 'false';
			} else {
				val = componentObject[prop];
			}

			properties.push(
				isArray ? val : (JSON.stringify(prop) + ': ' + val)
			);
		}

		properties = properties.join(",\n");

		return isArray
			? ('[' + properties + ']')
			: ('{' + properties + '}');
	}

	generateRoutesDefinitionFile(routes) {
		let imports = '';
		let routesDef = [];

		routes.forEach(route => {
			const compName = 'Page' + this.fileNameToComponentName(route.fileName);

			// route component definition
			let routeComponentDef;
			if (route.lazy) {
				let webpackConfig = '';
				if (route.chunkName) {
					webpackConfig += `/* webpackChunkName: ${JSON.stringify(route.chunkName)} */ `;
				}
				routeComponentDef = `() => import(${webpackConfig}'./components/${route.fileName}')`;
			} else {
				imports = `import ${compName} from 'vue';\n`;
				routeComponentDef = compName;
			}

			// meta tags
			let meta = [];
			if (route.config.metaTitle) {
				meta.push('title:' + JSON.stringify(route.config.metaTitle));
			}
			if (route.config.metaKeywords) {
				meta.push('keywords:' + JSON.stringify(route.config.metaKeywords));
			}
			if (route.config.metaDescription) {
				meta.push('description:' + JSON.stringify(route.config.metaDescription));
			}

			// final vue route definition
			let routeDef = `path: "${route.config.route}"`;
			routeDef += `, component: ${routeComponentDef}`
			routeDef += `, name: "${compName}"`
			if (meta.length) {
				routeDef += `, meta:{ ${meta.join(',')} }`
			}

			routesDef.push('{' + routeDef + '}');
		});

		// we need empty element for Vue routing
		imports += 'import Empty from "./components/empty";\n';
		routesDef.push(`{path: '*', component: Empty, name: 'PageEmpty'}`);

		return imports + 'export default [\n' + routesDef.join(',\n') + '\n];';
	}

	generateDynamicPageComponentsFile(pageComponents, componentRootPath) {
		let compImports = `import Vue from 'vue';\n`;
		let compRegs = '';
		let compDef = [];

		// registered to load props for component
		require.extensions['.vue'] = (module, filename) => {
			try {
				module._compile(this.parseVueComponent(
					fs.readFileSync(filename, 'utf8'), filename, {
						presets: [
							[babelPreset, {targets: {node: 'current'}}],
						]
					}
				), filename);
			} catch (error) {
				throw new Error(error);
			}
		};

		pageComponents.forEach(comp => {
			const compName = 'Page' + this.fileNameToComponentName(comp.fileName);
			// generate page components imports
			if (comp.lazy) {
				let webpackConfig = '';
				if (comp.chunkName) {
					webpackConfig += `/* webpackChunkName: ${JSON.stringify(comp.chunkName)} */ `;
				}
				compRegs += `Vue.component('${compName}', () => import(${webpackConfig}'./components/${comp.fileName}'));\n`;
			} else {
				compImports += `import ${compName} from './components/${comp.fileName}';\n`;
				compRegs += `Vue.component('${compName}', ${compName});\n`;
			}

			// detect properties list
			let compProps;
			try {
				compProps = require(
					path.join(process.cwd(), componentRootPath, comp.fileName + '.vue')
				).default.props || null;
				compProps = compProps
					? JSON.stringify(Array.isArray(compProps) ? compProps : Object.keys(compProps))
					: 'null';
			} catch (error) {
				console.log('skip props filtering for ' + compName);
				compProps = 'null';
			}


			// generate page definition
			compDef.push(`{\ncomponentName: "${compName}",\nprops:${compProps},\npageIdentifier: ${JSON.stringify(comp.pageIdentifier)}\n}`);
		})

		return compImports + '\n' + compRegs + '\n' + 'export default [\n' + compDef.join(`,\n`) + '\n];';
	}

	fileNameToComponentName(fileName) {
		// in case it's path
		if (fileName.includes('/')) {
			fileName = fileName.substring(fileName.lastIndexOf('/') + 1).replace('.vue', '');
		}

		const compName = fileName
			.replace(/[\-\.]/g, ' ')
			.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
				return index === 0 ? word.toLowerCase() : word.toUpperCase();
			})
			.replace(/\s+/g, '');

		return compName[0].toUpperCase() + compName.slice(1);
	}

	generateScriptIfNeeded(componentString, config, fileName) {
		// Parse file content
		let component = VueTemplateCompiler.parseComponent(componentString);
		if (component.script) {
			return componentString;
		}
		if (!component.template) {
			componentString = '<template>' + componentString + '</template>';
		}

		const compName = this.fileNameToComponentName(fileName);
		const props = config.context ? Object.keys(config.context) : [];

		return componentString + `\n<script>export default {name:"${compName}",props:${JSON.stringify(props)}}</script>`;
	}

	uniqueComponentFileName(rootDir, fileName) {
		let exportPath = path.join(rootDir, `${fileName}.vue`);

		for (let i = 2; i < 100; i++) {
			if (!fs.existsSync(fileName)) {
				return fileName;
			}
			fileName = fileName + i;
			exportPath = path.join(rootDir, `${fileName}.vue`);
		}
	}

	loadVueComponentStyles(componentPath, processedFiles) {
		processedFiles = processedFiles || [];
		if (processedFiles.includes(componentPath)) {
			return '';
		}
		processedFiles.push(componentPath);

		let stylesContent = '';
		try {
			const componentFolder = componentPath.substring(0, componentPath.lastIndexOf('/') + 1);
			const componentContent = fs.readFileSync(componentPath, 'utf8');
			const component = VueTemplateCompiler.parseComponent(componentContent);
			if (component && component.styles && component.styles.length > 0) {
				stylesContent = component.styles.reduce((allStyles, style) => {
					let styleContent = style.content || '';
					if (style.src) {
						if (style.src.endsWith('.scss') || style.lang === 'scss') {
							styleContent = sass.renderSync({file: componentFolder + style.src}).css.toString();
						}
					} else if (style.lang === 'scss') {
						styleContent = sass.renderSync({
							data: styleContent,
							includePaths: [componentFolder]
						}).css.toString();
					}

					return allStyles + styleContent;
				}, '');
			}

			this.getImportVueFiles(componentContent, componentFolder).forEach(importFileName => {
				stylesContent += this.loadVueComponentStyles(importFileName, processedFiles)
			});
		} catch (e) {
			console.error(e);
			// ignore
		}

		return stylesContent || null;
	}

	getImportVueFiles(content, directory) {
		const matches = content.matchAll(/import [A-z0-9_]+ from ["']([A-z0-9\/\-._]+?)["']/g);

		let filePathList = [];
		for (const match of matches) {
			const filePath = path.join(directory, match[1] + '.vue');
			if (fs.existsSync(filePath)) {
				filePathList.push(filePath);
			}
		}

		return filePathList;
	}

	generateComponentScript(componentPath) {
		const componentFolder = componentPath.substring(0, componentPath.lastIndexOf('/') + 1);

		let config = {};
		fs.readdirSync(componentFolder).forEach(file => {
			if (file.endsWith('.config.json')) {
				config = JSON.parse(fs.readFileSync(componentFolder + file, 'utf8'));
			}
		});

		const compName = this.fileNameToComponentName(componentPath);
		const props = config.context ? Object.keys(config.context) : [];

		return `export default { name:"${compName}", props:${JSON.stringify(props)} }`;
	}

	parseVueComponent(content, path = '', babelOptions) {
		// Parse file content
		let component = VueTemplateCompiler.parseComponent(content);
		if (!component.template && !component.script) {
			component = VueTemplateCompiler.parseComponent('<template>' + content + '</template>');
		}

		const template = component.template ? component.template.content : null;
		let scriptContent = component.script ? component.script.content : this.generateComponentScript(path);

		// move template to script
		if (template) {
			scriptContent = scriptContent.replace(
				/export default {/,
				`export default {template: ${JSON.stringify(template)},`
			);
		}

		return Babel.transform(scriptContent, Object.assign({filename: path}, babelOptions)).code;
	}
}

module.exports = new VueFilesHelper;
