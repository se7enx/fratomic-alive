'use strict';

// build based on handlebars adapter example

const fs = require('fs');
const {Adapter, utils} = require('@frctl/fractal');
const Babel = require('@babel/core');
const babelPreset = require('babel-preset-env');
const Vue = require('vue');
const VueServerRenderer = require('vue-server-renderer');
const VueTemplateCompiler = require('vue-template-compiler');
const VueFilesHelper = require('./helper/VueFilesHelper');
let _; // lazy require('lodash');

class FractalVueAdapter extends Adapter {
	constructor(source, app, config) {
		super(null, source);

		this._app = app;
		this._config = config;
		this._appConfig = Object.assign({}, this._app.config(), {docs: null});

		Vue.mixin({
			methods: {
				path(path) {
					if (!this._env || this._env.server) {
						return path;
					}
					_ = _ || require('lodash');

					return utils.relUrlPath(
						path,
						_.get(this._env.request || this._request, 'path', '/'),
						fractal.web.get('builder.urls')
					);
				}
			}
		});

		this._babelOptions = Object.assign({
			presets: [[babelPreset, {targets: {node: 'current'}}]]
		}, this._config.babel);

		this._vueRenderer = VueServerRenderer.createRenderer();

		// As soon a source changes, the vue component definition needs to be updated
		source.on('updated', this.clearRequireCache.bind(this));

		require.extensions['.vue'] = (module, filename) => {
			try {
				module._compile(VueFilesHelper.parseVueComponent(
					fs.readFileSync(filename, 'utf8'), filename, this._babelOptions
				), filename);
			} catch (error) {
				throw new Error(error);
			}
		};
	}

	render(path, str, context, meta) {
		meta = meta || {};
		let VueComponent = require(path).default;

		// render server-side
		if (!meta.env || !meta.env.request || meta.env.request.query.static !== 'no') {
			let renderer = this._vueRenderer;
			if (meta.target) {
				renderer = VueServerRenderer.createRenderer({
					template: this.loadPageTemplate(meta.target.title)
				});
			}

			const vueInstance = new Vue({render: el => el(VueComponent, {props: context})});

			return renderer.renderToString(vueInstance)
				.then(result => {
					let styles = VueFilesHelper.loadVueComponentStyles(path);
					if (styles) {
						styles = `<style>${styles}</style>`;
						if (result.includes('</body>')) {
							result = result.replace(/<\/body>/i, styles + '</body>');
						} else {
							result += styles;
						}
					}

					return result;
				})
				.catch(err => {
					console.error(err);
					return err;
				});
		}

		// render client-side
		return new Promise(resolve => {
			const compName = 'appcomponentpreview' + Math.ceil(Math.random() * 10000);
			VueComponent = VueFilesHelper.generateComponentCode(VueComponent).replace('</script>', '</sc"+"ript>');
			const propsContent = JSON.stringify(context).replace('</script>', '</sc"+"ript>');

			let res = `<${compName}></${compName}>`;
			res += '<script type="application/javascript">';
			res += `new Vue(
				{render: h => h(
					${VueComponent},
					{props: ${propsContent}})
				}).$mount('${compName}')`;
			res += `</script>`;

			const styles = VueFilesHelper.loadVueComponentStyles(path);
			if (styles) {
				res += `<style>${styles}</style>`;
			}

			if (meta.target) { // it's preview template
				let template = this.loadPageTemplate(meta.target.title);
				if (meta.target.title) {
					template = template.replace('{{ title }}', meta.target.title);
				}
				res = template.replace('<!--vue-ssr-outlet-->', res);
			}

			return resolve(res);
		});
	}

	clearRequireCache() {
		Object.keys(require.cache).forEach(key => {
			if (key.includes('.vue')) {
				delete require.cache[key];
			}
		});
	}

	loadPageTemplate(title) {
		const layoutFile = this._appConfig.components.path + '/_pagelayout.html';
		if (fs.existsSync(layoutFile)) {
			return fs.readFileSync(layoutFile, 'utf8').replace('{{ title }}', title || '');
		}

		return '<html><head></head><body><!--vue-ssr-outlet--></body></html>';
	}
}

module.exports = config => {
	config = config || {};

	return {
		register(source, app) {
			return new FractalVueAdapter(source, app, config);
		}
	};
};
